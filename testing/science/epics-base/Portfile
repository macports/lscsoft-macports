# -*- coding: utf-8; mode: tcl; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- vim:fenc=utf-8:ft=tcl:et:sw=4:ts=4:sts=4
# $Id$

PortSystem			1.0

name				epics-base
version				3.14.12.4
revision			1
categories			science
platforms			darwin
maintainers			ligo.org:jonathan.hanks
license				EPICS
description			EPICS Base ${version}
long_description	This is the main core of EPICS, comprising the build system and tools, common and OS-interface libraries, \
					Channel Access client and server libraries, static and run-time database access routines, the database \
					processing code, and standard record, device and driver support. \
					This version has been patched to include the LIGO long channel name support.
conflicts			ligo-remote-access-monolithic
homepage			http://www.aps.anl.gov/epics/

# where it is going to live
set dest_prefix /src/epics
set epicsdir ${destroot}${prefix}${dest_prefix}

distfiles			baseR${version}.tar.gz:base \

master_sites		http://www.aps.anl.gov/epics/download/base/:base

checksums			baseR${version}.tar.gz \
					rmd160	1e1e7a6d4a58fb4cb95e94a1da0877e63a628724 \
					sha256	0ea4a74e83ff73d68b64de89bedf7c42102edc2cc69c69fd49eb35c04dc9e3de

worksrcdir			base-${version}

depends_lib			port:readline \
					port:openmotif
patch.pre_args		-p2
patchfiles			patch-0001-base${version}-long-strings.diff \
					patch-0002-Enabling-the-use-of-macports-includes-and-libraries.diff

build.cmd			gnumake
build.env			EPICS_HOST_ARCH=darwin-x86

use_configure		no

post-extract {
	system -W ${workpath} "ln -s base-${version} base"
}

post-patch {
	reinplace	"s|@@PREFIX@@|${prefix}|" ${workpath}/base-${version}/configure/os/CONFIG_SITE.darwinCommon.darwinCommon
}

destroot {
	file mkdir ${epicsdir}
	system -W ${workpath} "cp -a base-${version} ${epicsdir}"
	system -W ${epicsdir} "ln -s base-${version} base"
}

post-destroot {
	set epics_bin ${epicsdir}/base-${version}/bin
	set epics_lib ${epicsdir}/base-${version}/lib
	set epics_src ${epicsdir}/base-${version}/src

	# build a list of areas where binaries or libraries that need some path rewritting might live at
	set patterns [list ${epics_bin}/darwin-x86/*]
	set patterns [lappend patterns ${epics_lib}/darwin-x86/*]
	set patterns [lappend patterns ${epics_lib}/perl/5*/*/*]
	set patterns [lappend patterns ${epics_src}/*/O.darwin-x86/*]
	set patterns [lappend patterns ${epics_src}/*/*/O.darwin-x86/*]
	set patterns [lappend patterns ${epics_src}/*/*/*/O.darwin-x86/*]

	# use the install_name_tool application to rewrite paths since epics hardcodes path information
	foreach pattern $patterns {
		foreach fname [glob ${pattern} ] {
			if {[file isfile ${fname}] != 1} {
				continue
			}
			if {[catch {file link ${fname}}] == 0} {
				continue
			}
			if {[catch {exec otool -L ${fname}}] != 0} {
				continue
			}
			set lines [exec otool -L ${fname}]

			foreach line [split ${lines} "\n"] {
				set line [string trim ${line}]
				if {[string first ".dylib" ${line}] < 0} {
					continue
				}
				set line [string range ${line} 0 [string first ".dylib" ${line}]+5]
				if {[string first ${workpath} ${line}] != 0} {
					continue
				}

				set old_rel_path [string range ${line} [string length ${destroot}] [string length ${line}]]
				set cur_rel_path [string range ${fname} [string length ${destroot}] [string length ${fname}]]
				if {${old_rel_path} == ${cur_rel_path}} {
					exec install_name_tool -id ${cur_rel_path} ${fname}
				} else {
					set updated ${prefix}${dest_prefix}[string range ${line} [string length ${workpath}] [string length ${line}]]
					exec install_name_tool -change ${line} ${updated} ${fname}
				}
			}
		}
	}

	# link components into the file system in the appropriate places
	foreach pattern [list ${epics_bin}/darwin-x86/*] {
		foreach fname [glob ${pattern}] {
			set rel_path [string range ${fname} [string length ${destroot}${prefix}] [string length ${fname}]]
			#puts "ln -s ..${rel_path} [file tail $fname]"
			system -W ${destroot}${prefix}/bin "ln -s ..${rel_path} [file tail $fname]"
		}
	}
}
