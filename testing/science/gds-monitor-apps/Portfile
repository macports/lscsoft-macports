# -*- coding: utf-8; mode: tcl; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- vim:fenc=utf-8:ft=tcl:et:sw=4:ts=4:sts=4

PortSystem    1.0
PortGroup     conflicts_build 1.0
PortGroup     active_variants 1.1

name          gds-monitor-apps
version       2.19.0
categories    science
platforms     darwin
maintainers   {@emaros ligo.org:ed.maros} openmaintainer
license       GPL

description   LSC Algorithm Library
long_description \
  LIGO Scientific Collaboration Algorithm Library containing core \
  routines for gravitational wave data analysis.

homepage      https://www.lsc-group.phys.uwm.edu/daswg/projects/lalsuite.html
master_sites  http://software.ligo.org/lscsoft/source/

checksums     rmd160  00f2cbceba275db2b3465c83cdc75a5f6dad44f0 \
              sha256  af7345d1a81e6d84e03b8c5d12b926b2ae4c9e12fa525c3ddc8fc4919235bcf8 \
              size    1601374

depends_build-append \
              port:autoconf \
              port:automake \
              port:pkgconfig

depends_lib   port:gds

#------------------------------------------------------------------------
# Fix things after PortGroup python
#------------------------------------------------------------------------
use_configure      yes
use_parallel_build yes
configure.args --enable-dtt \
               --enable-root-objects

#------------------------------------------------------------------------
# Use root 6, C++ 2014 for High Sierra and beyond
# else root 5, C++ 2011
#------------------------------------------------------------------------
if {${os.platform} eq "darwin" && ${os.major} >= 17 } {
    compiler.cxx_standard   2014

    depends_lib-append      port:root6
} else {
    compiler.cxx_standard   2011

    depends_lib-append      port:root5
}

#------------------------------------------------------------------------
# The macports version of sasl2 leads to a 5 minute latency for each
# socket connection.
#------------------------------------------------------------------------

livecheck.type   regex
livecheck.url    ${master_sites}
livecheck.regex  {gds-monitor-apps-(\d+(?:\.\d+)*).tar.gz}
