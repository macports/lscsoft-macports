# -*- coding: utf-8; mode: tcl; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- vim:fenc=utf-8:ft=tcl:et:sw=4:ts=4:sts=4

PortSystem    1.0
PortGroup     conflicts_build 1.0
PortGroup     active_variants 1.1

name          gds-swig
version       2.19.0
categories    science
platforms     darwin
maintainers   {@emaros ligo.org:ed.maros} openmaintainer
license       GPL

description   LSC Algorithm Library
long_description \
  LIGO Scientific Collaboration Algorithm Library containing core \
  routines for gravitational wave data analysis.

homepage      https://www.lsc-group.phys.uwm.edu/daswg/projects/lalsuite.html
master_sites  http://software.ligo.org/lscsoft/source/

checksums     rmd160  93bed66ccf00e7a3089c5587161ed1561cf3a0fa \
              sha256  e3dd1fc18a5c0c2efefe857cfcbbf6f3bfea3f476309a1d66ffd3443c4038d24 \
              size    414042

depends_build-append \
              port:pkgconfig \
              port:autoconf \
              port:automake \
              port:swig \
              port:swig-python

depends_lib   port:gds

#------------------------------------------------------------------------
# Fix things after PortGroup python
#------------------------------------------------------------------------
regsub -all -- "-swig$" ${name} {} basename

use_configure      yes
use_parallel_build yes

configure.args --enable-online \
               --disable-python \
    			--enable-dtt
post-destroot {
    if {${subport} eq "${name}"} {
        set fp [open "${workpath}/README.txt" w+]
        puts ${fp} "This is the SWIG stub for GDS language bindings"
        close ${fp}
        set docdir ${prefix}/share/doc/${name}
        xinstall -d ${destroot}${docdir}
        xinstall -m 644 ${workpath}/README.txt ${destroot}${docdir}
    }
}

#========================================================================
# Create subports for each supported Python version
#========================================================================
foreach v {27 35 36 37 38} {
  set python.version          ${v}
  set python.branch           [string range ${python.version} 0 end-1].[string index ${python.version} end]
  set python.bin              ${prefix}/bin/python${python.branch}
  set python.prefix           ${frameworks_dir}/Python.framework/Versions/${python.branch}
  set python.site_packages    "${python.prefix}/lib/python${python.branch}/site-packages"
  set python.pkgname          ""

  subport py${v}-${basename} {
    categories-append         python
    description               Python ${python.version} bindings for ${description}
    long_description          ${long_description} This package provides Python \
                              ${python.version} bindings, modules, and scripts.

    depends_build-append      port:swig-python
    depends_lib-append        port:python${python.version}
    depends_lib-append        port:py${python.version}-numpy

    configure.python          ${python.bin}
    configure.args-replace    --disable-python --enable-python

    destroot.target           install

    post-destroot {
      if {${subport} eq "py27-${basename}"} {
        foreach script [glob -tails -nocomplain -directory ${destroot}${python.prefix}/bin *] {
          file link -symbolic ${destroot}${prefix}/bin/${script} ../Library/Frameworks/Python.framework/Versions/${python.version}/bin/${script}
        }
      }
    }

    livecheck.type            none
  }
}
#------------------------------------------------------------------------
# Use C++ 2014 for High Sierra and beyond
# else C++ 2011
#------------------------------------------------------------------------
if {${os.platform} eq "darwin" && ${os.major} >= 17 } {
    use_autoreconf yes
    use_automake yes

    compiler.cxx_standard   2014
} else {
    compiler.cxx_standard   2011
}

#------------------------------------------------------------------------
#------------------------------------------------------------------------

livecheck.type   regex
livecheck.url    ${master_sites}
livecheck.regex  {gds-swig-(\d+(?:\.\d+)*).tar.gz}
