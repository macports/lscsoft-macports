# -*- coding: utf-8; mode: tcl; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- vim:fenc=utf-8:ft=tcl:et:sw=4:ts=4:sts=4

PortSystem    1.0
PortGroup     compiler_blacklist_versions 1.0
PortGroup     cmake 1.0

name          ldas-tools-utilities
version       2.6.4
categories    science
license       GPL-2+
platforms     darwin
maintainers   {@emaros ligo.org:ed.maros} openmaintainer

license       GPL-2+
description   LDAS Tools utilities derived from multiple LDAS Tools libraries
long_description \
@PROJECT_DESCRIPTION_LONG_0 \
@PROJECT_DESCRIPTION_LONG_1

homepage      https://wiki.ligo.org/DASWG/LDASTools
master_sites  http://software.ligo.org/lscsoft/source/

checksums     rmd160  642079128d560de360c8483248fbb72a65b07555 \
              sha256  c31b176d02c6647313f3253d14056c41d5f69e204abe75d9480f935d8f2f2d2d \
              size    2008758

depends_build  port:pkgconfig \
               port:ldas-tools-cmake
depends_lib    port:ldas-tools-frameAPI \
               port:ldas-tools-diskcacheAPI

cmake.out_of_source yes

configure.args

# requires clang from Xcode5 or higher to build
compiler.blacklist-append {clang < 500.2.75} llvm-gcc-4.2 gcc-4.2

use_parallel_build yes

pre-fetch {
    if {${os.platform} eq "darwin" && ${os.major} < 11} {
        ui_error "${name} only runs on Mac OS X 10.7 or greater."
        return -code error "incompatible Mac OS X version"
    }
}

#------------------------------------------------------------------------

livecheck.type   regex
livecheck.url    ${master_sites}
livecheck.regex  {${name}-(\d+(?:\.\d+)*).tar.gz}
